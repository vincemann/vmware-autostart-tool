import subprocess

def execute(cmd, logError=False, logCommand=False):
    if logCommand:
        print("executing command: " + cmd)
    try:
        output = subprocess.check_output(
            cmd, stderr=subprocess.STDOUT, shell=True,
            universal_newlines=True)
    except subprocess.CalledProcessError as exc:
        if logError:
            print("Status : FAIL" + "\nreturncode: " + str(exc.returncode) + "\nmessage: " + str(exc.output))
    else:
        return output
