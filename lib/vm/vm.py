from lib.util.AutoString import auto_str

@auto_str
class Vm:

    def __init__(self, configLine, delimiter, onlyPath=False):
        parts = configLine.split(delimiter)
        if not onlyPath:
            if len(parts) < 3:
                print("vmline: " + str(parts) + " has invalid syntax")
                raise SyntaxError("vmline: " + str(parts) + " has invalid syntax")
        self.vmPath = parts[0]
        if not onlyPath:
            self.vmUserName = parts[1]
            self.vmIp = parts[2]
