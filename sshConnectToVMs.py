import os
import sys
from lib import Shell
from lib.vm import vmConfigFileReader

if len(sys.argv) < 2:
    print("usage: sshConnectToVMs.py <vmConfigFilePath>")
    exit(1)
vmConfigFilePath = sys.argv[1]


scriptDir = os.path.dirname(os.path.realpath(__file__))
vms = vmConfigFileReader.readVMConfigFile(vmConfigFilePath)

for vm in vms:
    print("starting ssh connection terminal on vm: " + str(vm))
    try:
        Shell.execute("gnome-terminal -x sh -c '"+str(scriptDir)+"/startSSHConnectionTerminal.sh " + " " + vm.vmUserName + "@" + vm.vmIp+"'",
                      logCommand=True)
    except Exception as e:
        print("error ssh connecting to vm, skipping..." + " caused by: " + str(e))
