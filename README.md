__Description:__  
Tools are provided to automatically *start*, *stop* your virtual machines and *ssh connect* to them.  
The tools read a config file, containing the vms you want to start and connect to.  

__Usage:__  
starts all vms listed in the config file  
`python3.5 startVMs.py /path/to/your/vmConfigFile    `

starts all vms listed in the config file   
`python3.5 stopVms.py /path/to/your/vmConfigFile    `   

opens a gnome terminal for every entry in the config file containing an ip and username (see example configFile below)  
and tries to ssh connect until succeeded   
`python3.5 sshConnectToVMs.py /path/to/your/vmConfigFile     ` 

__Dependencies:__  
python3.5   
gnome-terminal   
vmrun   
ssh   

__Prerequisites:__    
You must be using vmware.
You will need to confige key-based ssh authentication for sshConnectToVMs.py to work.  
-> so when you run `ssh username@ip` in a terminal, it is not prompting you for a password.  
You need to make all *.sh files executable (sudo chmod +x /path/of/this/downloaded/repo/\*.sh)


__ConfigFile Example:__  
_\#       syntax: path [\<D\> username \<D\> ip]_  
  
_\#       this vm will be started/stopped when startVMs.py/ stopVms.py is called_  
_\#       this vm will __NOT__ be ssh connected on when sshConnectToVMs.py is called_  
`/path/to/my/.vmxFile`  
_\#       this vm will be started/stopped when startVMs.py/ stopVms.py is called_  
_\#       this vm will be ssh connected on when sshConnectToVMs.py is called   
`/path/to/my/.vmxFile <D> sshUsername <D> ip`  
_\#       more entries could follow_  


